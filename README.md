# systemSCTI
O sistema terá como objetivo:
* Inscrição dos ouvintes
  - Verificar se o mesmo é portador de necessidade especiais
  - Gerar Boleto
  - Paypal
  - Cadastro no minicurso
    - Verificação de disponibilidade na turma
    - Envio de email para com os requisitos do minicurso
    - Gerador do certificado online
* Usuário
  - Membros da organização
    - Financeiro (Transparência)
      - Envio das imagens dos comprovantes
  - Cadastros de Erros e Acertos por Ano
  - Cadastro de metas para cada membro
  - Cadastro de de departamentos
    - Presidente
    - Departamento Financeiro
    - Departamento de Marketing
    - Departamento de Projetos
    - Departamento de Vendas
  - Envio de email para os membros a cada 2 meses para a instalação dos softwares dos minicursos já fechados
  - Palestrante
    - Envio dos slides
    - Cadastro ( Sistema enviará a cada 2 meses a data e o dia da palestra do mesmo )
      - Solicitação de confirmação
    - Envio dos requisitos para os minicursos
      - Todos os softwaresnecessários para serem instalados.
      
#Tecnologias
-> Ruby on Rails
-> BDD
  -> RSPEC
-> Git
